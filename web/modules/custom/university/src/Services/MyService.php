<?php

namespace Drupal\university\Services;


/**
 * Provides my special service.
 */


class MyService {
  /**
   * Gets hello world message.
   *
   * @return string
   *   The message.
   */
  public function myLogic() {
    return "My Logic Result";
  }
}
