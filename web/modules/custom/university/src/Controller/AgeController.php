<?php

namespace Drupal\University\Controller;

/**
 *implements method for base routes
 */
class AgeController {

  public function compareAge() {
    $message = \Drupal::config('system.maintenance')->get('message');
    return [
      '#theme' => 'university-making-age',
      '#ageTyson' => 20,
      '#agePetro' => 50,
    ];
  }
}
