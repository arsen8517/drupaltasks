<?php

/**
 *implements method for base routes
 */
namespace Drupal\University\Controller;

class UniversityController {

  public function viewTest() {
    return [
      //"#markup" => "Hello world, university!",
      '#theme' => 'university_template',
      '#variable1' => 'Hello',
      '#variable2' => 'World',
    ];
  }
}
