<?php

namespace Drupal\University\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\university\Services\MyService;
use Psr\Container\ContainerInterface;

/**
 *implements method for base routes
 */
class SpeedController extends ControllerBase {

  protected $my_service;

  public static function create(ContainerInterface $countainer) {
      return new static (
        $countainer->get('university.say_hello')
      );
  }
  public function __construct(MyService $my_service) {
    $this->my_service = $my_service;
  }
  public function limitSpeed() {
     // $service = \Drupal::service('university.say_hello');
    return [
      '#type' => 'markup',
      '#markup' => $this->my_service->myLogic()
       //'#markup' => $service->myLogic(),
    ];
  }
}
