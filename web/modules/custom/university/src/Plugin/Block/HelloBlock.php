<?php
/**
 * @file
 * Main file for hooks and custom functions.
 */
namespace Drupal\university\Plugin\Block;
use Drupal\Core\Block\BlockBase;
/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Hello World"),
 * )
 */
class HelloBlock extends BlockBase {

  public function build() {
    $config = \Drupal::config('system.site');
    $message = $config->get('slogan');

//    $form_state = new FormState();
//    $form_state->set('some_value', 'Hello World');
    $form = \Drupal::formBuilder()->getForm('Drupal\foo\Form\FooForm');

    return [
     // '#markup' => $this->t('Hello block'.$message),
      '#markup' => $this->t('My Form here'.$form),
      ];
  }

}
